import * as React from "react";

export class Wheel extends React.Component<{
    width: number,
    height: number,
    scrolling: any,
    frameBorder: number
}> {
    render() {
        return (
            <iframe
                src="https://wheeldecide.com/e.php?c1=Stringhylla&c2=En+apelsin&c3=Vattenkokare&c4=En+kram&c5=H%C3%B6rlurar&c6=Trosor+Bj%C3%B6rn+B.&c7=Kindle&c8=N%C3%A4ssk%C3%B6lj&col=winter&cols=620001,8A0000&t=Julklappsjul&time=10&tcol=FFFFFF&remove=1"
                width={this.props.width} height={this.props.height} scrolling={this.props.scrolling}
                frameBorder={this.props.frameBorder}/>
        );
    }
}


