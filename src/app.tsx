import * as React from "react";
import * as ReactDOM from "react-dom";
import './app.css';
import {Game} from "./components/game";

ReactDOM.render(
    <div>

        <div className="wrapper">
            <header className="main-head">
                <img className="image" src="./assets/img/christmas.svg"/>
            </header>
            <nav className="main-nav">

            </nav>
            <article className="content ">

                <Game />

            </article>

            <div className="ad"></div>
            <footer className="main-footer">
                <a rel="nofollow" href="https://www.vecteezy.com">Vector Illustration by vecteezy.com</a>
            </footer>
        </div>

    </div>,
    document.getElementById('root')
);