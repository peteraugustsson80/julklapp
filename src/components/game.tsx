import * as React from "react";
import {Wheel} from "./wheel";
import '../app.css';

interface MyProps {
}
interface MyState {isStarted: boolean
}

export class Game extends React.Component<MyProps, MyState> {
    constructor(props) {
        super(props);
        this.handleLoginClick = this.handleLoginClick.bind(this);
        this.handleLogoutClick = this.handleLogoutClick.bind(this);
        this.state = {isStarted: false};
    }

    handleLoginClick() {
        this.setState({isStarted: true});
    }

    handleLogoutClick() {
        this.setState({isStarted: false});
    }

    render() {
        const isStarted = this.state.isStarted;

        let button = null;
        if (isStarted) {
            button = <BackButton onClick={this.handleLogoutClick}/>;
        } else {
            button = <StartaButton onClick={this.handleLoginClick}/>;
        }
        return (
            <div>
                {button}
                <StartGame isStarted={isStarted}/>
            </div>
        );
    }
}

function UserGreeting(props) {
    return <div>

        <div className="content_text fixbackground">
            <h3>
                Regler
            </h3>
            <ul>
                <li>Snurra hjulet
                </li>
                <li>
                    Varje gåva som dyker upp kommer att plockas bort från listan.
                </li>
                <li>
                    Till slut återstår endast en gåva.
                </li>
                <li>
                    Gåvan är min julklapp till dig.
                </li>
            </ul>
        </div>

        <Wheel frameBorder={0} width={400} height={450} scrolling="yes"/>

        <aside className="side">

        </aside>
    </div>;
}

function StartGame(props) {
    const isStarted = props.isStarted;
    if (isStarted) {
        return <UserGreeting />;
    }
    return null;
}

function StartaButton(props) {
    return (
        <button className="btn" onClick={props.onClick}>
            Starta
        </button>
    );
}

function BackButton(props) {
    return (
        <button className="btn" onClick={props.onClick}>
            Back
        </button>
    );
}
